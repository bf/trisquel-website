
# SendDrupal

A simple mail client that sends mailing list subscriptions to Drupal forums.

## Usage

* Enable the sendmailman drupal module, see README in its own section of this repo.
* Set up an email account on a server with IMAPS, and subscribe it to any lists
that you want to mirror on a Drupal 9 site. It is best to use an address that 
cannot be guessed, to make sure it only ever receives expected messages.
* Configure the auth and other values on the config.ini file. You would need to
obtain an api key on the Drupal site, for user Trisquel (admin)
* Run the daemon. It would connect to the IMAP server and process unseen messages
that are already in the inbox or that arrive while running.
  * If the message is the first on a thread, it would create a new node
  * If the message is a reply, it would create a comment to the matching thread
  * Thread matching is done by the sendmailman drupal module metadata tracking
