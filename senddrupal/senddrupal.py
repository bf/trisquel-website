#!/usr/bin/python3
#
#    Copyright (C) 2023  Rubén Rodríguez <ruben@trisquel.org>
#
#    This program is free software; you can redistribute it and/or modify
#    it under the terms of the GNU General Public License as published by
#    the Free Software Foundation; either version 2 of the License, or
#    (at your option) any later version.
#
#    This program is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU General Public License for more details.
#
#    You should have received a copy of the GNU General Public License
#    along with this program; if not, write to the Free Software
#    Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301 USA

# A daemon that connects to an IMAP server and process incoming mail
# to send it to be published on the trisquel forums

import imapclient
import ssl
import email
import requests
import re
import random
import time
from socket import gethostbyname, gaierror
import configparser

config = configparser.ConfigParser()
config.read('config.ini')

IMAP_HOST = config.get('settings', 'IMAP_HOST')
IMAP_PORT = config.get('settings', 'IMAP_PORT')
USERNAME = config.get('settings', 'USERNAME')
PASSWORD = config.get('settings', 'PASSWORD')
DRUPAL_BASE_URL = config.get('settings', 'DRUPAL_BASE_URL')
DRUPAL_API_KEY = config.get('settings', 'DRUPAL_API_KEY')

IDLE_TIMEOUT = (60 * 29)  # 29 minutes, in case imap server has a 30 min timeout limit

# Set headers for REST API request
headers = {
    'Content-Type': 'application/json',
    'api-key': DRUPAL_API_KEY,
}


def create_user(name, mail):
    print(f'Created placeholder user:"{name}" with email: {mail}')
    if name == '' or mail == '':
        return -1
    description = "User profile created automatically from a message sent to a mailing list "\
        "at https://listas.trisquel.info\n\n"\
        "The owner of the originating email address can take over this account by "\
        f"recovering the password at {DRUPAL_BASE_URL}/user/password and changing the "\
        "username/avatar/description."
    node_data = {'name': [{'value': name}],
                 'mail': [{'value': mail}],
                 'profile_description1': [{'value': description}]}
    response = requests.post(f'{DRUPAL_BASE_URL}/entity/user?_format=json',
                             json=node_data, headers=headers)
    if response.status_code == 201:
        json = response.json()
        return json['uid'][0]['value']
    return -1


def send_to_drupal(message, body, textformat):
    metadata = get_metadata('msgid', message['message-id'])
    if metadata == -1:
        print('Error: cannot access the rest api! {DRUPAL_BASE_URL}/admin/config/services/rest')
        return False
    if metadata != []:
        print('This comes from drupal, stopping')
        return True

    forums = {'Trisquel-test': 50,
              'Freedom-misc': 2078,
              'Trisquel-benutzer': 701,
              'Trisquel-users': 50,
              'Trisquel-usuarios': 29,
              'Trisquel-utilisateurs': 69}

    # find the user id or create placeholder user
    mail = re.sub('>$', '', re.sub('^.*<', '', message['from']))
    uid = find_uid(mail)
    if uid == -1:   # email not registered in drupal, create user
        username = re.sub('<.*', '', message['from']).strip()
        username = ''
        if username == '':
            username = re.sub('@.*', '', mail)
        result = find_username(username)
        if result == -1:   # username not taken
            uid = create_user(username, mail)
        else:
            uid = create_user('%s_%s from lists' % (username, random.randint(10, 99)), mail)

    msgid = message['message-id']

    if 'list-id' in message:
        list_id = message['list-id']
    else:
        print('Message from unknown source %s' % message['from'])
        return True

    for key in forums:
        if key.lower() in list_id:
            known_address = True
            # Assign to the correct forum
            tid = forums[key]
            # Clean up the title
            subject = re.sub('[%s] ' % key, '', message['subject'])
            subject = re.sub('^[R,r]e: ', '', subject)

    # Remove mailing list signature and trailing blank lines
    pattern = r'.*_______________________________________________.*(?:\n(?:.*\n){0,3})?'
    body = re.sub(pattern, '', body)
    body = body.rstrip('\r\n> ')
    body = body.rstrip('\n>')
    body = body.rstrip('>')
    body = body.rstrip()

    # Check if email is a reply or a new thread
    if 'In-Reply-To' in message:  # Publish as a comment on the matching thread
        metadata = get_metadata('msgid', message['in-reply-to'])
        cid = 0
        if metadata != []:
            if metadata[0]['cid'] != 0:  # publish as child of comment cid
                pid = metadata[0]['cid']
            nid = metadata[0]['nid']
        else:
            nid = find_nid(subject)
            pid = 0

        node_data = {'entity_id': [nid],
                     'uid': [{'target_id': uid}],
                     'pid': [{'target_id': pid}],
                     'entity_type': [{'value': 'node'}],
                     'status': {'value': '1'},
                     'comment_type': [{'target_id': 'comment_forum'}],
                     'field_name': [{'value': 'comment_forum'}],
                     'subject': [{'value': subject}],
                     'comment_body': [{'value': body, 'format': textformat}]
                     }
        response = requests.post(f'{DRUPAL_BASE_URL}/comment?_format=json',
                                 json=node_data, headers=headers)
        if response.status_code == 201:
            json = response.json()
            newcid = json['cid'][0]['value']
            if not insert_metadata(msgid, nid, newcid, pid, uid, 0, tid):
                print('ERROR inserting metadata!')
                return False
            print(f'Published comment {DRUPAL_BASE_URL}/en/comment/reply/node/'
                  f'{nid}/comment_forum/{newcid}')
            return True
        else:
            print(f'Error publishing comment data {node_data} \
                    \nResponse code: {response.status_code} - {response.text}')
    else:  # Publish email as a new node to Drupal
        node_data = {'type': [{'target_id': 'forum'}],
                     'uid': [{'target_id': uid}],
                     'taxonomy_forums': [{'target_id': tid}],
                     'title': [{'value': subject}],
                     'body': [{'value': body}],
                     }
        response = requests.post(f'{DRUPAL_BASE_URL}/node?_format=json',
                                 json=node_data, headers=headers)
        if response.status_code == 201:
            json = response.json()
            nid = json['nid'][0]['value']
            if not insert_metadata(msgid, nid, 0, 0, uid, 0, tid):
                print('ERROR inserting metadata!')
                return False
            print(f'Published node {DRUPAL_BASE_URL}/node/{nid}')
            return True
        else:
            print(f'Error publishing node: {node_data}')
    return False


# Retrieve the highest nid that matches the title string
def find_nid(title):
    response = requests.get(f'{DRUPAL_BASE_URL}/jsonapi/node/forum?filter[title][value]=%s'
                            % title, headers=headers)
    if response.status_code == 200 and response.json()['data'] != []:
        return response.json()['data'][-1]['attributes']['drupal_internal__nid']
    else:
        return -1


def find_user(field, value, attr):
    response = requests.get(f'{DRUPAL_BASE_URL}/jsonapi/user/user?filter[%s][value]=%s'
                            % (field, value), headers=headers)
    if response.status_code == 200 and response.json()['data'] != []:
        return response.json()['data'][0]['attributes'][attr]
    else:
        return -1


def find_uid(mail):
    return find_user('mail', mail, 'drupal_internal__uid')


def find_username(username):
    return find_user('name', username, 'name')


def insert_metadata(msgid, nid, cid, pid, uid, mid, tid):
    """
    msgid: the email message-id
    nid: node id
    cid: comment id
    pid: parent comment id, 0 if it is a node
    uid: user id
    mid: mailbox id (from listhandler, no longer needed)
    tid: taxonomy id (forum id)
    """
    data = {'msgid': msgid,
            'nid': nid,
            'cid': cid,
            'pid': pid,
            'uid': uid,
            'mid': mid,
            'tid': tid,
            }
    response = requests.post(f'{DRUPAL_BASE_URL}/sendmailman', json=data, headers=headers)
    return response.status_code == 200


def get_metadata(field, value):
    response = requests.get(f'{DRUPAL_BASE_URL}/sendmailman/%s/%s'
                            % (field, value), headers=headers)
    if response.status_code == 200:
        return response.json()
    else:
        return -1


def process_messages():
    new_messages = client.search('UNSEEN')
    if new_messages:
        for msg_id in new_messages:
            email_data = client.fetch(msg_id, 'BODY.PEEK[]')
            msg = email.message_from_bytes(email_data[msg_id][b'BODY[]'])

            text_plain = ''
            base64_content = ''
            text_html = ''
            for part in msg.walk():
                payload = part.get_payload(decode=True)
                if payload:
                    try:
                        content = payload.decode('utf-8')
                    except Exception as e:
                        print(e)
                        pass
                if part.get_content_type() == 'text/plain' and \
                   part.get('Content-Transfer-Encoding') == 'base64':
                    base64_content = content
                elif part.get_content_type() == 'text/html':
                    text_html = content
                elif part.get_content_type() == 'text/plain':
                    text_plain = content

            date = time.strftime("%Y-%m-%d %H:%M:%S")
            print('--\n%s - Processing message Id: %s From: %s To: %s Subject: %s'
                  % (date, msg_id, msg['from'], msg['to'], msg['subject']))

            result = False
            if base64_content != '':
                result = send_to_drupal(msg, base64_content, 'messaging_plain_text')
            elif text_html != '':
                result = send_to_drupal(msg, text_html, 'html')
            elif text_plain != '':
                result = send_to_drupal(msg, text_plain, 'messaging_plain_text')
            else:
                print('Error, message body is empty! Ignoring message')
                client.set_flags([msg_id], ['\\SEEN'])
            if result:
                res = client.set_flags([msg_id], ['\\SEEN'])


while True:
    try:
        context = ssl.create_default_context()
        client = imapclient.IMAPClient(IMAP_HOST, port=IMAP_PORT, ssl=True,
                                       ssl_context=context, timeout=30)
        client.login(USERNAME, PASSWORD)
        client.select_folder('INBOX')

        print('Process mailbox')
        process_messages()
        print('Done processing mailbox, now watching for push messages')

        while True:
            try:
                client.idle()
                responses = client.idle_check(timeout=IDLE_TIMEOUT)

                if responses:
                    for response in responses:
                        if response[1].startswith(b'EXISTS'):
                            client.idle_done()
                            process_messages()
                client.idle_done()
            except Exception as e:
                client.shutdown()
                break

    except Exception as e:
        date = time.strftime("%Y-%m-%d %H:%M:%S")
        exception_type = type(e).__name__
        if exception_type == 'gaierror' and e.errno == -2:
            print("Imap server: Name or service not known, fix your settings")
            break
        if exception_type == 'gaierror' and e.errno == -3:
            print(f"{date} - Cannot connect to server, attempting to reconnect")
            time.sleep(10)
