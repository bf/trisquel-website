# SendMailman Drupal 9 module

A module that sends new forum nodes/comments to a mailing list

## Usage:

This module works together with the *senddrupal* daemon. You also need
to configure a mechanism to allow the mail server on the drupal site to
send messages directly to the mailman queues, such as using a separate
list processing tool like *listadmin*, available in the Trisquel repos.

The module also provides a /sendmailman REST api endpoint, that allows
*senddrupal* to query and insert metadata information for mail thread
matching. You would need to set the endpoint configuration via the
rest api ui in Drupal, enabling the endpoints **Sendmailman**, **Content**,
**Comment** and **User**, with methods: GET+POST formats: json 
authentication: key_auth 

TO-DO: Set the information for list addresses, and the matching with
forum taxonomies via module configuration forms. They are currently
harcoded in the module.
