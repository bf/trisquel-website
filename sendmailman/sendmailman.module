<?php

#    Copyright (C) 2023  Rubén Rodríguez <ruben@trisquel.org>
#
#    This program is free software; you can redistribute it and/or modify
#    it under the terms of the GNU General Public License as published by
#    the Free Software Foundation; either version 2 of the License, or
#    (at your option) any later version.
#
#    This program is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU General Public License for more details.
#
#    You should have received a copy of the GNU General Public License
#    along with this program; if not, write to the Free Software
#    Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301 USA

use Drupal\Core\Mail\MailManagerInterface;
use Drupal\Component\Utility\SafeMarkup;
use Drupal\Component\Utility\Html;
use Drupal\node\Entity\Node;
use Drupal\user\Entity\User;


function sendmailman_mail($key, &$message, $params) {
  $options = array(
    'langcode' => $message['langcode'],
  );
  if ($key == 'node_insert' or $key == 'comment_insert') {
    $message['subject'] = $params['title'];
    $message['body'][] = Html::escape($params['message']);
    $message['headers']['From'] = $params['from'];
    $message['headers']['Message-Id'] = $params['message-id'];
    $message['headers']['X-Sent-By'] = 'sendmailman';
  }
  if ($key == 'comment_insert') {
    if (array_key_exists('in-reply-to', $params))
      $message['headers']['In-Reply-To'] = $params['in-reply-to'];
  }

}

function _metadata_get($field, $value, $order="DESC") {
  $query = \Drupal::database()->select('sendmailman')
           ->fields('sendmailman', ['lid','msgid','nid','cid','pid','uid','mid','tid'])
           ->condition($field, $value, '=')
           ->orderBy('lid', $order)
           ->execute();
  $results = $query->fetchAll();

  $data = [];
  foreach ($results as $result) {
    $data[] = [
    'lid' => $result->lid,
    'msgid' => $result->msgid,
    'nid' => $result->nid,
    'cid' => $result->cid,
    'pid' => $result->pid,
    'uid' => $result->uid,
    'mid' => $result->mid,
    'tid' => $result->tid,
    ];
  }
  return $data;
}

function _metadata_save($msgid, $nid, $pid, $cid, $uid, $tid){
  $data = array(
    'msgid' => "<$msgid>",
    'nid' => $nid,
    'pid' => $pid,
    'cid' => $cid,
    'uid' => $uid,
    'tid' => $tid,
  );
  $insert = \Drupal::database()->insert('sendmailman')
    ->fields($data)
    ->execute();
}

function _address_from_tid($tid){
  $tid = 50;
  if ($tid == 50)
	  return "trisquel-users-en@listas.trisquel.info";
  if ($tid == 29)
	  return "trisquel-usuarios-es@listas.trisquel.info";
  if ($tid == 69)
	  return "trisquel-utilisateurs-fr@listas.trisquel.info";
  if ($tid == 701)
	  return "trisquel-benutzer-de@listas.trisquel.info";
  if ($tid == 1894)
	  return "trisquel-utilizatori-it@listas.trisquel.info";
  if ($tid == 2065)
	  return "trisquel-utenti-ro@listas.trisquel.info";
  if ($tid == 2078)
	  return "trisquel-misc-en@listas.trisquel.info";
  if ($tid == 803)
	  return "trisquel-troll-en@listas.trisquel.info";
}

function sendmailman_node_insert(\Drupal\node\NodeInterface $node) {
  $user = User::load($node->getOwnerId());
  $mailManager = \Drupal::service('plugin.manager.mail');
  $module = 'sendmailman';
  $key = 'node_insert';
  $params['message'] = $node->get('body')->value;
  $params['title'] = $node->getTitle();
  //$params['from'] = $user->getEmail();
  $accountname = $user->getAccountName();
  $params['from'] = "$accountname <forums@trisquel.org>";
  $langcode = \Drupal::currentUser()->getPreferredLangcode();
  $send = true;

  $URI=$_SERVER['REQUEST_URI'];
  if ($URI == "/node?_format=json"){
    \Drupal::logger('sendmailman')->notice("Ignoring message sent by daeon $URI");
    return;
  }
    \Drupal::logger('sendmailman')->notice("wtf $URI");

  $lid = 1;
  $nid = $node->id();
  $pid = 0;
  $cid = 0;
  $uid = $node->getOwnerId();
  $term_reference_field = $node->get('taxonomy_forums');
  $tid = $term_reference_field->target_id;
  //$to = "trisquel-test@listas.trisquel.info";
  $uuid = \Drupal::service('uuid')->generate();
  $msgid = "$uuid@trisquel.org";
  $params['message-id'] = "listhandler=$lid&site=www.trisquel.org&nid=$nid&pid=$pid&cid=$cid&uid=$uid&tid=$tid&$msgid";
  _metadata_save($params['message-id'], $nid, $pid, $cid, $uid, $tid);

  $result = $mailManager->mail($module, $key, $to, $langcode, $params, NULL, $send);
    if ($result['result'] !== true) {
        \Drupal::logger('sendmailman')->error("Error sending ");
    } else {
        \Drupal::logger('sendmailman')->notice("Sent");
    }
}

function sendmailman_comment_insert(\Drupal\comment\CommentInterface $comment) {

  $user = User::load($comment->getOwnerId());
  $node = Node::load($comment->getCommentedEntityId());

  $mailManager = \Drupal::service('plugin.manager.mail');
  $module = 'sendmailman';
  $key = 'comment_insert';
  $params['message'] = $comment->get('comment_body')->value;
  $params['title'] = $node->getTitle();
  //$params['from'] = $user->getEmail();
  $accountname = $user->getAccountName();
  $params['from'] = "$accountname <forums@trisquel.org>";
  $langcode = \Drupal::currentUser()->getPreferredLangcode();
  $send = true;

  $URI=$_SERVER['REQUEST_URI'];
  if ($URI == "/comment?_format=json"){
    \Drupal::logger('sendmailman')->notice("Ignoring message sent by daeon $URI");
    return;
  }
    \Drupal::logger('sendmailman')->notice("wtf $URI");

  $lid = 1;
  $nid = $node->id();
  $pid = 0;
  $parent=$comment->getParentComment();
  if ($parent!=null)
	  $pid=$parent->id();
  $cid = $comment->id();
  $uid = $node->getOwnerId();
  $term_reference_field = $node->get('taxonomy_forums');
  $tid = $term_reference_field->target_id;
  //$to = "trisquel-test@listas.trisquel.info";
  $uuid = \Drupal::service('uuid')->generate();
  $msgid = "$uuid@trisquel.org";
  $params['message-id'] = "listhandler=$lid&site=www.trisquel.org&nid=$nid&pid=$pid&cid=$cid&uid=$uid&tid=$tid&$msgid";
  if ($pid != 0){
    // there is a parent, get its msgid by matching the curent parent id to the parent's own cid
    $metadata = _metadata_get('cid', $pid);
  }else{
    // no parent, get the msgid of the node (sort asc to get first entry)
    $metadata = _metadata_get('nid', $nid, "ASC");
  }
  $params['in-reply-to'] = $metadata[0]['msgid'];
  _metadata_save($params['message-id'], $nid, $pid, $cid, $uid, $tid);

  $result = $mailManager->mail($module, $key, $to, $langcode, $params, NULL, $send);
    if ($result['result'] !== true) {
        \Drupal::logger('sendmailman')->error("Error sending ");
    } else {
        \Drupal::logger('sendmailman')->notice("Sent");
    }
}


